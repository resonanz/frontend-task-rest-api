# Front-End Hiring Task backend server

 A RESTful API for the resonanz Front-End Hiring Task

## dependancies

* Node
* npm

## Installation

1. Clone the project.
2. In the project root folder run `npm install`.
3. create a `.env` file in the project root, the file should include the following environment variables:

### Example .env file

```
# SERVER
HOST=localhost
PORT=8081

```

## development

run `npm run dev`.

## Endpoints

### GET /power-plants

Returns a list of power plants with their location and latest output

#### Response
```
[
  {
    "bundesnetzagentur_number": <string>,
    "company_name": <string>,
    "name": <string>,
    "plz": <int>,
    "city": <string>,
    "address": <string>,
    "state": <string>,
    "start_date": <string>,
    "status": <enum[IN_SERVICE]>,
    "type": <enum[WIND_ONSHORE | WIND_OFFSHORE | HYDRO | BIOMASS]>, 
    "net_nominal_power_mw": <int>,
    "lastUpdated": "2019-12-21T00:00:00.000",
    "production_value_kw": <int>,
    "aggregated_production_kw": <int>,
    "location": {
      "longitude": <int>,
      "latitude: <int>
    }
  },
...
}
```
