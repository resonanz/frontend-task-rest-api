const data = require('./power-plants.json');


function getRandomInRange(from, to, fixed) {
  return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
  // .toFixed() returns string, so ' * 1' is a trick to convert to number
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const generateRandomLocations = () => {
  const locations = [];
  for (let i = 0; i < 50; i++) {
    locations.push({
      latitude: getRandomInRange(51, 52, 3),
      longitude: getRandomInRange(7, 8, 3),
    });
  }

  return locations;
};

const LOCATIONS = generateRandomLocations();

const addPowerPlantMetrics = (powerPlant, i) => {
  const prodValue = getRandomInt(0, 3000);
  return {
    ...powerPlant,
    lastUpdated: `2019-12-${getRandomInt(0,31)}T00:00:00.000`,
    production_value_kw: prodValue,
    aggregated_production_kw: prodValue * 7 / getRandomInt(1, 2),
    location: LOCATIONS[i],
  };
};

const TYPES = ['BIOMASS', 'WIND_ONSHORE', 'WIND_OFFSHORE', 'HYDRO'];

module.exports.getAll = () => {
  return data.filter(pp => TYPES.includes(pp.type)).slice(0, 50).map(addPowerPlantMetrics);
};
