const handlers = require('./handlers');

const {
  failAction,
} = require('../../utils/helpers');


const register = async (server) => {
  server.route([
    {
      method: 'GET',
      path: '/power-plants',
      handler: handlers.getAll,
      options: {
        validate: {
          failAction,
        },
      },
    },
  ]);
};

const plugin = {
  register,
  name: 'powerPlantsApi',
  version: '1.0',
};

exports.plugin = plugin;
